<?php

$pendapatan_kamar=$database->select('transaksi_kamar','*',
		['tgl_checkout[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

$total_pendapatan_kamar=$database->sum('transaksi_kamar','total',
		['tgl_checkout[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

$pendapatan_fasilitas=$database->select('transaksi_fasilitas',
		['[><]reservasi'=>'id_reservasi'],
		['transaksi_fasilitas.id_reservasi',
			'transaksi_fasilitas.total',
			'reservasi.tgl_checkout'],
		['reservasi.tgl_checkout[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

	$total_pendapatan_fasilitas=$database->sum('transaksi_fasilitas',
		['[><]reservasi'=>'id_reservasi'],
		['transaksi_fasilitas.total'],
		['reservasi.tgl_checkout[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

$total_pendapatan=$database->select('transaksi_pendapatan','*',
		['tgl_checkout[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

	$total_pendapatan_semua=$database->sum('transaksi_pendapatan','total',
		['tgl_checkout[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

$penggajian_karyawan=$database->select('penggajian_karyawan','*',
		['Tanggal_Pembayaran[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

	$total_penggajian_karyawan=$database->sum('penggajian_karyawan','total',
		['Tanggal_Pembayaran[<>]'=>[$_GET['mulai'],$_GET['akhir']]]);

	$keuntungan=$total_pendapatan_semua - $total_penggajian_karyawan;

	$pajak= $total_pendapatan_semua * 0.10;

	$grand_keuntungan=$keuntungan-$pajak;


?>