<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">ACCOUNTING</li>
      <li>
              <a href="index.php">
                <i class="fa fa-dashboard"></i> <span>Beranda</span>
              </a>
            </li>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Laporan Pendapatan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="?module=laporan/pendapatan-kamar"><i class="fa fa-bed"></i> Pendapatan Kamar</a></li>
                <li><a href="?module=laporan/pendapatan-fasilitas"><i class="fa fa-cutlery"></i> Pendapatan Fasilitas</a></li>
                <li><a href="?module=laporan/total-pendapatan"><i class="fa fa-calculator"></i> Total Pendapatan</a></li>
                <li><a href="?module=laporan/statistik-pendapatan"><i class="fa fa-bar-chart"></i> Statistik</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-external-link"></i>
                <span>Laporan Pengeluaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="?module=laporan/penggajian-karyawan"><i class="fa fa-group"></i> Gaji Karyawan</a></li>
                <li><a href="?module=laporan/total-pengeluaran"><i class="fa fa-calculator"></i> Total Pengeluaran</a></li>
                <li><a href="?module=laporan/statistik-pengeluaran"><i class="fa fa-bar-chart"></i> Statistik</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Laporan Keuntungan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="?module=laporan/total-keuntungan"><i class="glyphicon glyphicon-piggy-bank"></i> Total Keuntungan</a></li>
                <li><a href="?module=laporan/statistik-keuntungan"><i class="fa fa-bar-chart"></i> Statistik</a></li>
              </ul>
            </li>
		</ul>
	</section>
</aside>