<header class="main-header">
	<a href="index2.html" class="logo">
		<span class="logo-mini"><b>K</b>8</span>
		<span class="logo-lg"><b>Kelompok </b>8</span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<a href="#" class="navbar-brand">
			<b>KOMA HOTEL</b>
			<span class="small">Teknologi Informasi 2014</span>
		</a>
	</nav>
</header>