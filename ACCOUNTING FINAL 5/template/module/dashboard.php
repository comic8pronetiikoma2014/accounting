<section class="content-header">
	<h1>Beranda <span class="small">Selamat datang</span></h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-sm-4">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>Laporan</h3>
					<p>Pendapatan</p>
				</div>
				<div class="icon">
					<i class="fa fa-book"></i>
				</div>
				<a class="small-box-footer" href="?module=laporan/total-pendapatan">Lihat Selengkapnya</a>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="small-box bg-red">
				<div class="inner">
					<h3>Laporan</h3>
					<p>Pengeluaran</p>
				</div>
				<div class="icon">
					<i class="fa fa-external-link"></i>
				</div>
				<a class="small-box-footer" href="?module=laporan/total-pengeluaran">Lihat Selengkapnya</a>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>Laporan</h3>
					<p>Keuntungan</p>
				</div>
				<div class="icon">
					<i class="fa fa-money"></i>
				</div>
				<a class="small-box-footer" href="?module=laporan/total-keuntungan">Lihat Selengkapnya</a>
			</div>
		</div>
	</div>
</section>