<script src="template/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<script src="template/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="template/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="template/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
<script src="template/dist/js/app.min.js" type="text/javascript"></script>
<script src="template/dist/js/demo.js" type="text/javascript"></script>
<script src="template/plugins/select2/select2.full.min.js" type="text/javascript"></script>
<script src="template/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		$('.nama-tamu').select2();
		$('#checkin').datepicker();
        $('#checkout').datepicker();
	});
</script>