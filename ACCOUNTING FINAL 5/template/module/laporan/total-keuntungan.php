<?php

include('component/com-laporan.php');

?>

<section class="content-header">
	<h1>Laporan Keuntungan Total Hotel KOMA</h1>
</section>

<section class="content">
	<form action="" method="post">
		<div class="row">
			<div class="col-sm-3">
				<div class="form-group">
					<input id="checkin" data-date-format="yyyy-mm-dd" class="form-control" name="tanggal-start" placeholder="Dari Tanggal" />
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<input id="checkout" data-date-format="yyyy-mm-dd" class="form-control" name="tanggal-end" placeholder="Sampai Tanggal" />
				</div>
			</div>
			<div class="col-sm-3">
				<button class="btn btn-success" type="submit" name="laporan">Lihat Laporan</button>
			</div>
		</div>
	</form>
	<?php if(isset($_POST['laporan'])) { ?>
	<div class="box">
		<div class="box-body">
			<h3>Rincian Keuntungan</h3><hr/>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Keterangan</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Pendapatan</td>
						<td>Rp <?php echo number_format($total_pendapatan_semua); ?></td>
					</tr>
					<tr>
						<td>Pengeluaran</td>
						<td>Rp <?php echo number_format($total_penggajian_karyawan); ?></td>
					</tr>
						<tr>
							<td> </td>
							<td> </td>
						</tr>
					<tr>							
						<td><b>Keuntungan</b></td>
						<td><b>Rp <?php echo number_format($keuntungan); ?></b></td>
					</tr>
					<tr>							
						<td><b>PPN 1%</b></td>
						<td><b>Rp <?php echo number_format($pajak); ?></b></td>
					</tr>
				</tbody>
				<tfoot>
					<tr>							
						<?php
							echo '<td><b>Total Keuntungan</b></td>';
							echo '<td class="lead text-red">
									<span class="col-sm-6">Rp '.number_format($grand_keuntungan).'</span>
									<span class="lead col-sm-6">
										<a class="btn btn-danger" href="?report=cetak-total-keuntungan&mulai='.$newDate1.'&akhir='.$newDate2.'" target="_blank">
											Cetak Laporan
										</a>
									</span>
								</td>';	
						?>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<?php } ?>
</section>