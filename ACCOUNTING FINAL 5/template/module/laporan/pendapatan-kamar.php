<?php

include('component/com-laporan.php');

?>

<section class="content-header">
	<h1>Laporan Pendapatan Kamar</h1>
</section>

<section class="content">
	<form action="" method="post">
		<div class="row">
			<div class="col-sm-3">
				<div class="form-group">
					<input id="checkin" data-date-format="yyyy-mm-dd" class="form-control" name="tanggal-start" placeholder="Dari Tanggal" />
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<input id="checkout" data-date-format="yyyy-mm-dd" class="form-control" name="tanggal-end" placeholder="Sampai Tanggal" />
				</div>
			</div>
			<div class="col-sm-3">
				<button class="btn btn-success" type="submit" name="laporan">Lihat Laporan</button>
			</div>
		</div>
	</form>
	<?php if(isset($_POST['laporan'])) { ?>
	<div class="box">
		<div class="box-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Tanggal</th>
						<th>Pendapatan</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($pendapatan_kamar as $pendapatan_kamar) { ?>
					<tr>
						<td><?php echo date("d M Y", strtotime($pendapatan_kamar['tgl_checkout'])); ?></td>
						<td>Rp <?php echo number_format($pendapatan_kamar['total']); ?></td>
					</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td><span class="lead">Total Pendapatan : <b>Rp <?php echo number_format($total_pendapatan_kamar); ?></b></span></td>
						<td>
							<span class="lead">
								<a class="btn btn-danger" href="?report=cetak-pendapatan-kamar&<?php echo 'mulai='.$newDate1.'&akhir='.$newDate2.'"'; ?>" target="_blank">Cetak Laporan</a>
							</span>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<?php } ?>
</section>