<!DOCTYPE html>
<html>
  <?php include('module/header.php'); ?>
  
  <body onload="window.print();">
    <!-- Site wrapper -->
    <div class="wrapper">
      <section class="invoice">
        <h2 class="page-header">
          KOMA HOTEL
          <span class="small">Teknologi Informasi 2014</span>
          <span class="lead text-red pull-right"><b>LAPORAN KEUANGAN</b></span>
        </h2>
        <h6>Jln. Universitas No. 9A. Kampus USU, Medan 20155.
          <br/><b>Telp :</b> 000 0000 0000<b> - Fax :</b> 000 0000 0000<b> - Email :</b> koma.tiusu2014@gmail.com
          <br/><b>www.komatiusu2014.co.id</b>
        </h6>
        <br/>
        <br/>

        <!-- Report Content -->
        <?php 
        
        $report=$_GET['report'];
        include('report/'.$report.'.php');
        
        ?>
        <!-- end:Report -->
      </section>
    </div>

    <!-- jQuery 2.1.4 -->
    <?php include('module/script.php'); ?>
  </body>
</html>
