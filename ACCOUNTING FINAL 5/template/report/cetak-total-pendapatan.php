<?php
	include('component/com-cetak.php');
?>

<div class="row">
	<div class="col-sm-6">
		Ditujukan Kepada :
		<address>
			<strong>Finance Departemen </strong>
		</address>
		Perihal :
		<address>
			<strong>Laporan Pendapatan Hotel KOMA </strong>
		</address>
	</div>
	<div class="col-sm-6">
		<b>PER-TANGGAL : </b><br/>
		<span class="lead"><?php echo date("d M Y", strtotime($_GET['mulai']))." s/d ".date("d M Y", strtotime($_GET['akhir'])); ?></span><br/><br/>
		<b>Tanggal Terbit :</b><br/>
		<span class="lead"><?php echo date('d M Y'); ?></span>
	</div>
</div>

<h3>RINCIAN LAPORAN</h3>
<table class="table table-bordered table-striped table-responsive">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th>Jenis Pendapatan</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($total_pendapatan as $total_pendapatan) { ?>
					<tr>
						<td><?php echo date("d M Y", strtotime($total_pendapatan['tgl_checkout'])); ?></td>
						<td>Reservasi Kamar & Fasilitas</td>
						<td>Rp <?php echo number_format($total_pendapatan['total']); ?></td>
					</tr>
					<?php } ?>
	</tbody>
</table>

<div class="row">
	<div class="col-sm-6">
		<p class="text-muted well well-sm no-shadow">
			<b>Catatan :</b> Mohon simpan laporan dengan baik.
		</p>
	</div>
	<div class="col-sm-6">
		<table class="table table-bordered table-responsive">
			<tr>
				<td><b>Total Pendapatan</b></td>
				<td><b>Rp <?php echo number_format($total_pendapatan_semua); ?></b></b></td>
			</tr>
		</table>
	</div>
</div>
