<?php
	include('component/com-cetak.php');
?>

<div class="row">
	<div class="col-sm-6">
		Ditujukan Kepada :
		<address>
			<strong>Finance Departemen </strong>
		</address>
		Perihal :
		<address>
			<strong>Laporan Keuntungan Hotel KOMA </strong>
		</address>
	</div>
	<div class="col-sm-6">
		<b>PER-TANGGAL : </b><br/>
		<span class="lead"><?php echo date("d M Y", strtotime($_GET['mulai']))." s/d ".date("d M Y", strtotime($_GET['akhir'])); ?></span><br/><br/>
		<b>Tanggal Terbit :</b><br/>
		<span class="lead"><?php echo date('d M Y'); ?></span>
	</div>
</div>

<h3>RINCIAN LAPORAN</h3>
<table class="table table-bordered table-striped table-responsive">
	<thead>
					<tr>
						<th>Keterangan</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Pendapatan</td>
						<td>Rp <?php echo number_format($total_pendapatan_semua); ?></td>
					</tr>
					<tr>
						<td>Pengeluaran</td>
						<td>Rp <?php echo number_format($total_penggajian_karyawan); ?></td>
					</tr>
						<tr>
							<td> </td>
							<td> </td>
						</tr>
					<tr>							
						<td><b>Keuntungan</b></td>
						<td><b>Rp <?php echo number_format($keuntungan); ?></b></td>
					</tr>
					<tr>							
						<td><b>PPN 1%</b></td>
						<td><b>Rp <?php echo number_format($pajak); ?></b></td>
					</tr>
				</tbody>
</table>

<div class="row">
	<div class="col-sm-6">
		<p class="text-muted well well-sm no-shadow">
			<b>Catatan :</b> Mohon simpan laporan dengan baik.
		</p>
	</div>
	<div class="col-sm-6">
		<table class="table table-bordered table-responsive">
			<tr>
				<td><b>Total Keuntungan</b></td>
				<td class="lead text-red"><b>Rp <?php echo number_format($grand_keuntungan); ?></b></b></td>
			</tr>
		</table>
	</div>
</div>