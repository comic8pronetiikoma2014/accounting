<?php
	include('component/com-cetak.php');
?>

<div class="row">
	<div class="col-sm-6">
		Ditujukan Kepada :
		<address>
			<strong>Finance Departemen </strong>
		</address>
		Perihal :
		<address>
			<strong>Laporan Penggajian Karyawan Hotel KOMA </strong>
		</address>
	</div>
	<div class="col-sm-6">
		<b>PER-TANGGAL : </b><br/>
		<span class="lead"><?php echo date("d M Y", strtotime($_GET['mulai']))." s/d ".date("d M Y", strtotime($_GET['akhir'])); ?></span><br/><br/>
		<b>Tanggal Terbit :</b><br/>
		<span class="lead"><?php echo date('d M Y'); ?></span>
	</div>
</div>

<h3>RINCIAN LAPORAN</h3>
<table class="table table-bordered table-striped table-responsive">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($penggajian_karyawan as $penggajian_karyawan) { ?>
					<tr>
						<td><?php echo date("d M Y", strtotime($penggajian_karyawan['Tanggal_Pembayaran'])); ?></td>
						<td>Rp <?php echo number_format($penggajian_karyawan['total']); ?></td>
					</tr>
					<?php } ?>
	</tbody>
</table>

<div class="row">
	<div class="col-sm-6">
		<p class="text-muted well well-sm no-shadow">
			<b>Catatan :</b> Mohon simpan laporan dengan baik.
		</p>
	</div>
	<div class="col-sm-6">
		<table class="table table-bordered table-responsive">
			<tr>
				<td><b>Total Pengeluaran</b></td>
				<td><b>Rp <?php echo number_format($total_penggajian_karyawan); ?></b></b></td>
			</tr>
		</table>
	</div>
</div>
