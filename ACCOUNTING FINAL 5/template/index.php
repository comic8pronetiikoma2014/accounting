<!DOCTYPE html>
<html>
	<?php include('module/header.php'); ?>

	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<?php 
				include('module/topbar.php');
				include('module/sidebar.php'); 
			?>

			<div class="content-wrapper">
				<?php 
					if(!empty($_GET['module'])) 
					{
						$module=$_GET['module'];

						include('module/'.$module.'.php');
					}
					else
					{
						include('module/dashboard.php');
					}
				?>
						
			</div>
				
			<?php include('module/footer.php'); ?>

			<div class="control-sidebar-bg"></div>
		</div>

		<?php include('module/script.php'); ?>
	</body>
</html>