<head>
    <meta charset="UTF-8">
    <title>KOMA HOTEL</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="template/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="template/bootstrap/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="template/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="template/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="template/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="template/dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>