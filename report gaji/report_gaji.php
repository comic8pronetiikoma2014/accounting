<?php
	include "koneksi.php";
	$nama_dokumen='Laporan Gaji Karyawan'; // Beri nama file PDF hasil
	
	define('_MPDF_PATH','MPDF/');
	include(_MPDF_PATH . "mpdf.php");
	
	$mpdf=new mPDF('utf-8', 'A4'); // Create new mPDF Document

	// Beginning Buffer to save PHP variables and HTML tags
	ob_start();

?>

<html>
	<head>
		<title> Laporan Gaji Karyawan </title>
	</head>
	<body>
		<h2>Gaji Karyawan Hotel Koma</h2><br>
		<table border="1">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Posisi</th>
					<th>Divisi</th>
					<th>Gaji Pokok</th>
					<th>Gaji Tambahan</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$query = mysql_query("SELECT karyawan.nama_lengkap, posisi.nama_posisi, divisi.nama_divisi, posisi.gaji_pokok, gaji.tambahan_gaji FROM karyawan, posisi, divisi, gaji WHERE posisi.id=karyawan.id_posisi && posisi.id_divisi=divisi.id && karyawan.id=gaji.id_karyawan");
					while($con = mysql_fetch_array($query)){
						echo "
							<tr>
								<td>".$con['nama_lengkap']."</td>
								<td>".$con['nama_posisi']."</td>
								<td>".$con['nama_divisi']."</td>
								<td>".$con['gaji_pokok']."</td>
								<td>".$con['tambahan_gaji']."</td>
							</tr>
						";
					}
				?>
			</tbody>
		</table>

		<h3> Total Pengeluaran Hotel </h3>
		<h4>Gaji Pokok + Gaji Tambahan = 

<?php
$gaji = mysql_query("SELECT SUM(gaji_pokok) AS totalpokok FROM posisi");
$row = mysql_fetch_assoc($gaji);

$tambahan = mysql_query("SELECT SUM(tambahan_gaji) AS totaltambah FROM gaji");
$rowt = mysql_fetch_assoc($tambahan);

$tes=$row['totalpokok'];
$test=$rowt['totaltambah'];
$jumlah=$tes + $test;
echo $tes." + ".$test." = ".$jumlah."</h4>";


$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');
exit;
?>
	</body>
</html>
